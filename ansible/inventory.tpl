[all:vars]
ansible_user=ec2-user
ansible_ssh_private_key_file="<path_to_your_aws_ssh_private_key>"
pipeline_deployment_ssh_key="<bitbucket_pipeline_ssh_pub_key>"
app_code_git="https://vasekch@bitbucket.org/vasekch/interview-demo.git"

GOLEMIO_API_URL=https://api.golemio.cz/v2/airqualitystations/
GOLEMIO_API_KEY=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhhY2thdGhvbkBnb2xlbWlvLmN6IiwiaWQiOjIsIm5hbWUiOiJIYWNrYXRob24iLCJzdXJuYW1lIjoiR29sZW1pbyIsImlhdCI6MTU4NDU0NDYzMSwiZXhwIjoxMTU4NDU0NDYzMSwiaXNzIjoiZ29sZW1pbyIsImp0aSI6IjVlNjU2NDQxLTA4OGUtNDYyYS1iMjUyLTFiNzI1OGU0ZGJkYSJ9.ypDAJirgEs8VBSauraFEoLTTtC6y_F8V1fheAHgzMos
AWS_S3_BUCKET_NAME=golemio-air-quality
AWS_ACCESS_KEY_ID=<aws_access_key>
AWS_SECRET_ACCESS_KEY=<aws_access_secret>
DATA_PROCESS_PERIOD_MINUTES="60"

[demo-host]
<IP_of_your_AWS_EC2_instance>     demo_home_dir="/app"


# NOTE
# GOLEMIO_API_KEY is freely available test key (may need updating from time to time)
# from https://operator-ict.gitlab.io/golemio/documentation/cs/otevrena-data-api/
# you should better generate your own on https://api.golemio.cz/api-keys/auth/sign-in

