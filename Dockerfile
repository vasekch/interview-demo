FROM python:3.8

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

# update pip to remove warning
RUN python -m pip install --upgrade pip

# Install pip requirements
ADD requirements.txt .
RUN python -m pip install -r requirements.txt

# switch to non-root user
RUN useradd appuser
RUN mkdir /app
RUN chown -R appuser /app
USER appuser
WORKDIR /app

# Default data process rate is once each 60 mins
ENV DATA_PROCESS_PERIOD_MINUTES 60

# install source codes
COPY --chown=appuser:appuser ./src /app

CMD ["python", "app.py"]
