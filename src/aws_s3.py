"""
Helper function to upload string data to S3 as a file
"""
import boto3
from botocore.exceptions import NoCredentialsError
from io import BytesIO

# AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY env vars used by boto3 by default


def upload_to_s3(text, bucket_name, file_name, content_type='text/plain'):
    """
    convert given text to file-like object and upload to S3 bucket
    """
    s3 = boto3.client('s3')
    try:
        s3.upload_fileobj(BytesIO(bytes(text, encoding='utf-8')), bucket_name, file_name, ExtraArgs={'ContentType': content_type})
        print("{} uploaded to S3 Bucket {}".format(file_name, bucket_name))
        return True
    except NoCredentialsError:
        print("S3 upload error: Credentials not available")
        return False
