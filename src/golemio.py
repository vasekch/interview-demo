"""
This file contains business logic for this demo, run this using app.py.

"""
import json
import requests
from datetime import datetime
from jinja2 import Template

from aws_s3 import upload_to_s3
from config import TZ


def process_data(api_url, api_key, s3_bucket_name):
    """
    Main business logic:
    1. retrieving data from API
    2. store input data as input.json on S3
    3. transform data for our purposes (simple format for template)
    4. generate output HTML
    5. publish output as index.html on S3
    """
    # 1
    # get data from remote API and save prettyfied json data to S3
    data = get_data(api_url, api_key)
    text = json.dumps(data, indent=4)
    # 2
    upload_to_s3(text, s3_bucket_name, 'input.json', 'application/json')
    # 3
    # transform data to internal format
    out_data = transform_data(data)
    # 4
    # generate HTML and save to S3
    out_html = render_data('output.html', out_data)
    # 5
    upload_to_s3(out_html, s3_bucket_name, 'index.html', 'text/html')


def get_data(api_url, api_key):
    """
    Get data from given URL and return as an object
    """
    # # DEBUG - bypass API call
    # with open('test_input.json') as f:
    #     print('Mocking API with test_data.json')
    #     return json.load(f)

    headers = {
        "X-Access-Token": api_key,
        "Content-Type": "application/json; charset=utf-8"
    }
    r = requests.get(api_url, headers=headers)
    print('API response', r.status_code)
    return r.json()


def transform_data(data):
    """
    Transforms data object from Golemio Air Pollution API (see ./test_input.json) to our internal format
    [
        {
            'id': 'ABREA',
            'name': 'Praha 6-Břevnov',
            'updated_at': '2020-05-24T10:20:00.298Z',
            'measurements': {
                'CO': '4',
                'CO2': '5.4',
                ...
            }
        },
        ...
    ]
    """
    print("Data transformation")
    out_data = []
    for item in data.get('features', []):
        out_item = {
            'id': item.get('properties', {}).get('id'),
            'name': item.get('properties', {}).get('name'),
            'updated_at': item.get('properties', {}).get('updated_at'),
            'measurements': {}
        }
        for m in item.get('properties', {}).get('measurement', {}).get('components', []):
            if m.get('type'):
                # append key to output measurements
                out_item['measurements'][m.get('type')] = {
                    'value': m.get('averaged_time', {}).get('value'),
                    'descr': 'avg last {}h'.format(m.get('averaged_time', {}).get('averaged_hours'))
                }
        # append compiled item to final output
        out_data.append(out_item)
    return out_data


def render_data(template_file, data):
    """
    Generates HTML output from given template file using data provided.
    Returns string
    """
    template = ''
    with open(template_file) as file:
        template = Template(file.read())
    return template.render(now=datetime.now(TZ), data=data)
