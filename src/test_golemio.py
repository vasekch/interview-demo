import json

TRANSFORM_RESULT = [
    {'id': 'ABREA', 'name': 'Praha 6-Břevnov', 'updated_at': '2020-05-22T22:20:00.271Z',
     'measurements': {'NO2': {'value': 42.8, 'descr': 'avg last 1h'}, 'PM10': {'value': 7, 'descr': 'avg last 1h'}}},
    {'id': 'ALERA', 'name': 'Letiště Praha', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 22.6, 'descr': 'avg last 1h'}, 'CO': {'value': 383.571, 'descr': 'avg last 8h'}, 'O3': {'value': 59.1, 'descr': 'avg last 1h'}, 'PM10': {'value': 14, 'descr': 'avg last 1h'}, 'PM2_5': {'value': 6, 'descr': 'avg last 1h'}}},
    {'id': 'ARIEA', 'name': 'Praha 2-Riegrovy sady', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'SO2': {'value': 1.3, 'descr': 'avg last 1h'}, 'NO2': {'value': 31.2, 'descr': 'avg last 1h'}, 'O3': {'value': 61.2, 'descr': 'avg last 1h'}, 'PM10': {'value': 10, 'descr': 'avg last 1h'}, 'PM2_5': {'value': 9, 'descr': 'avg last 1h'}}},
    {'id': 'AVYNA', 'name': 'Praha 9-Vysočany', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 54.7, 'descr': 'avg last 1h'}, 'O3': {'value': 42.5, 'descr': 'avg last 1h'}, 'PM10': {'value': 37, 'descr': 'avg last 1h'}}},
    {'id': 'ACHOA', 'name': 'Praha 4-Chodov', 'updated_at': '2020-05-22T22:20:00.271Z',
     'measurements': {'NO2': {'value': 13.6, 'descr': 'avg last 1h'}, 'PM10': {'value': 24, 'descr': 'avg last 1h'}}},
    {'id': 'ALIBA', 'name': 'Praha 4-Libuš', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'SO2': {'value': 2.7, 'descr': 'avg last 1h'}, 'NO2': {'value': 25.3, 'descr': 'avg last 1h'}, 'CO': {'value': 321.857, 'descr': 'avg last 8h'}, 'O3': {'value': 68.8, 'descr': 'avg last 1h'}, 'PM10': {'value': 16.4, 'descr': 'avg last 1h'}, 'PM2_5': {'value': 5.9, 'descr': 'avg last 1h'}}},
    {'id': 'ASROA', 'name': 'Praha 10-Šrobárova', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 28.5, 'descr': 'avg last 1h'}, 'PM10': {'value': 18, 'descr': 'avg last 1h'}, 'PM2_5': {'value': 9, 'descr': 'avg last 1h'}}},
    {'id': 'APRUA', 'name': 'Praha 10-Průmyslová', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 28.9, 'descr': 'avg last 1h'}, 'PM10': {'value': 11, 'descr': 'avg last 1h'}}},
    {'id': 'ALEGA', 'name': 'Praha 2-Legerova (hot spot)', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 99.1, 'descr': 'avg last 1h'}, 'CO': {'value': 620.75, 'descr': 'avg last 8h'}, 'PM10': {'value': 26, 'descr': 'avg last 1h'}, 'PM2_5': {'value': 12, 'descr': 'avg last 1h'}}},
    {'id': 'ASTOA', 'name': 'Praha 5-Stodůlky', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'O3': {'value': 63.4, 'descr': 'avg last 1h'}, 'PM10': {'value': 8, 'descr': 'avg last 1h'}, 'PM2_5': {'value': 6, 'descr': 'avg last 1h'}}},
    {'id': 'AKOBA', 'name': 'Praha 8-Kobylisy', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 26, 'descr': 'avg last 1h'}, 'O3': {'value': 67.4, 'descr': 'avg last 1h'}, 'PM10': {'value': 21, 'descr': 'avg last 1h'}}},
    {'id': 'ARERA', 'name': 'Praha 5-Řeporyje', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 59.9, 'descr': 'avg last 1h'}, 'PM10': {'value': 20, 'descr': 'avg last 1h'}, 'PM2_5': {'value': 10, 'descr': 'avg last 1h'}}},
    {'id': 'AVRSA', 'name': 'Praha 10-Vršovice', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 50.3, 'descr': 'avg last 1h'}, 'PM10': {'value': 28, 'descr': 'avg last 1h'}}},
    {'id': 'AKALA', 'name': 'Praha 8-Karlín', 'updated_at': '2020-05-22T22:20:00.271Z',
     'measurements': {'NO2': {'value': 61.6, 'descr': 'avg last 1h'}, 'PM10': {'value': 27, 'descr': 'avg last 1h'}}},
    {'id': 'AREPA', 'name': 'Praha 1-n. Republiky', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 30.8, 'descr': 'avg last 1h'}, 'PM10': {'value': 31, 'descr': 'avg last 1h'}}},
    {'id': 'ASUCA', 'name': 'Praha 6-Suchdol', 'updated_at': '2020-05-22T22:20:00.272Z',
     'measurements': {'NO2': {'value': 25.1, 'descr': 'avg last 1h'}, 'O3': {'value': 71.2, 'descr': 'avg last 1h'}, 'PM10': {'value': 7, 'descr': 'avg last 1h'}}}
]


def test_data_transform():
    """
    Unit test for data_transform check that testdata_input.json returns
    desired data (as per TRANSFORM_RESULT)
    """
    from golemio import transform_data
    with open('test_input.json') as json_file:
        test_data = json.load(json_file)
        assert transform_data(test_data) == TRANSFORM_RESULT
