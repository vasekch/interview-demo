"""
Please don't change this file, use ENV vars
"""
import os
from pytz import timezone

TZ = timezone('Europe/Prague')

DATA_PROCESS_PERIOD_MINUTES = float(os.environ.get('DATA_PROCESS_PERIOD_MINUTES', '60'))
GOLEMIO_API_URL = os.environ.get('GOLEMIO_API_URL')
GOLEMIO_API_KEY = os.environ.get('GOLEMIO_API_KEY')
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_S3_BUCKET_NAME = os.environ.get('AWS_S3_BUCKET_NAME')
