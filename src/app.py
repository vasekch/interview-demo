"""
This is a interview demo application

It periodically runs following routine every P minutes

1. download data from Golemio
2. Filter data for CO, CO2, NO into simple data object
3. Generate HTML page with data in a table.

GOLEMIO_API_URL and GOLEMIO_API_KEY evnironment vars must be set

Period (P) is float and can be provided with DATA_PROCESS_PERIOD_MINUTES environment
variable or as a first argument when running from command line (the later
has precedence).

If period is 0 routine is executed only single time (good for debugging)
"""
import asyncio
import functools
import os
import signal
import sys
from datetime import datetime

from golemio import process_data
from config import TZ, DATA_PROCESS_PERIOD_MINUTES, GOLEMIO_API_URL, GOLEMIO_API_KEY, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_S3_BUCKET_NAME


# checking all env vars
if not (AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY and AWS_S3_BUCKET_NAME and GOLEMIO_API_URL and GOLEMIO_API_KEY):
    print('AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_S3_BUCKET_NAME, GOLEMIO_API_URL and GOLEMIO_API_KEY',
          'environment vars must be defined')
    exit(20)


async def process_task():
    print("-" * 60)
    print("Processing started at {}".format(datetime.now(TZ).strftime("%Y-%m-%d %H:%M:%S %Z")))
    process_data(GOLEMIO_API_URL, GOLEMIO_API_KEY, AWS_S3_BUCKET_NAME)


def ask_exit(signame, loop):
    print("got signal %s: exit" % signame)
    loop.stop()


async def scheduler(period=0):
    while True:
        asyncio.create_task(process_task())
        if not period:
            return
        # period is in minutes
        await asyncio.sleep(period * 60)


async def main(period=0):
    print("=" * 60)
    print("Starting Golemio extract demo - Python3 asyncio")
    print(f"pid {os.getpid()}: send SIGINT or SIGTERM to exit.")

    # register exit signals
    loop = asyncio.get_running_loop()
    for signame in {'SIGINT', 'SIGTERM'}:
        loop.add_signal_handler(
            getattr(signal, signame),
            functools.partial(ask_exit, signame, loop))

    # main scheduler
    print("Scheduler period set to {} minutes".format(period))
    await scheduler(period)
    print("Scheduler exited")


if __name__ == '__main__':
    period = DATA_PROCESS_PERIOD_MINUTES

    # treat first argument as period (has precedence over env var)
    if len(sys.argv) > 1:
        try:
            period = float(sys.argv[1])
        except ValueError:
            print('First argument should be int or float')
            exit(10)

    try:
        asyncio.run(main(period))
    except RuntimeError as e:
        print(e)
