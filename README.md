# Prague Air Pollution #

This README contains documentation of the interview demo application, instructions how to run it on local machine and how it's deployed to AWS.

**tl;tr**

- source data: [Golemio API Air Quality CHMI doc](https://golemioapi.docs.apiary.io/#reference/air-quality/air-quality-stations-chmi/get-all-air-quality-chmi-stations)
- source data storage: [AWS S3 webhosting](http://golemio-air-quality.s3-website.eu-central-1.amazonaws.com/input.json)
- result data: [AWS S3 webhosting](http://golemio-air-quality.s3-website.eu-central-1.amazonaws.com/)
- app status - [Docker log on EC2](http://18.157.135.76/status.txt)
- CI/CD: [Bitbucket pipelines](https://bitbucket.org/vasekch/interview-demo/addon/pipelines/home)
- Provisioning: [CloudFormation template](https://bitbucket.org/vasekch/interview-demo/src/master/provision_ec2_in_vpc.cf.yml)

Result application is conteinerized with Docker - locally run using `docker-compose up` (needs ENV vars - see bellow) - on EC2 deployed in pure docker.

## Problem ##

**Goal**: Show the ability to automate the deployment of dockerized application

Infrastructure and tools:

- AWS EC2
- AWS S3
- Docker and your preferred docker image
- Ansible
- Python or shell

Task:

- Download regularly (e.g. daily / hourly) some dataset from the free data provider. If you don't know any, choose from:
    - https://github.com/CSSEGISandData/COVID-19/
    - https://openweathermap.org/current
- Store downloaded dataset to S3 bucket
- From every downloaded dataset, extract some specific data (eg data relevant for Czechia, Prague, ...)
- Display all extracted data using a single HTML page served from S3. A simple table is enough.

Instructions:

- Use well-known languages (preferable Python 3 or shell) to create scripts/application
- Create a docker to encapsulate the application logic
- Use latest Ansible to create roles and playbooks
- Put all your source code in a public git repository (e.g. Github)
- Use `README.md` file for the documentation (while evaluating we will use it to run the code)
- If you find problems, or not implement something, you should mention it there
- You don't need to provide automation for AWS infrastructure (EC2, S3) setup but you should document it

Bonus points:

- Replace EC2 with AWS serverless offering
- Document the next steps to make this small app being ready for production
- Automate even the infrastructure setup (cloudformation, terraform)
- Create a CI / CD pipelines
- Use your imagination and provide more than expected

### Analysis ###

Let's split this in two tasks, one to setup infrastructure and second to deal with the business logic.

#### Infrastructure ####

I choose bitbucket as our code repository. I'll setup pipelines for CI (each pullrequest run unit test automatically over the branch) and for CD (any update to master is automatically deployed).

I will create S3 bucket in AWS with web hosting features enabled. For hosting docker app, I'll use EC2 instance within separate VPC - this will be scripted using CloudFormation.

EC2 instance will provide application status (docker log) on HTTP:80.

#### Business logic ####

I choose Golemio API - Prague OpenData platform to retrieve information about current air pollution from different stations. I will transform input data into simplified format to print as a HTML table (filtering out the clutter).

I'll create Python3 application/script, that will use `requests` module to communicate with API, `jinja2` to generate HTML from the template and `boto3` module to connect to AWS S3. Execution steps are:

1. download data from API
2. store raw data on S3
3. transform data
4. generate HTML output
5. store output as `index.html` on S3


## Solution ##

Application output is available on AWS S3 Bucket: http://golemio-air-quality.s3-website.eu-central-1.amazonaws.com/

![](./doc/output.png)

Application backend status (docker log): http://18.157.135.76/status.txt

![](./doc/server_status.png)


### Code layout ###

Python application `./src/app.py` uses AsyncIO to wrap business logic from `./src/golemio.py` into a periodically running script with non-blocking sleeps - simple custom scheduler.

A simple unit test for data transformation is available in `./src/test_golemio.py`.

All configuration is read from ENV variables - see local development for details.


### Local development ###

You will need ENV vars to configure API and S3 connection. Create `.env` file with following:

```
GOLEMIO_API_URL=https://api.golemio.cz/v2/airqualitystations/
GOLEMIO_API_KEY=<golemio_api_key>
AWS_S3_BUCKET_NAME=golemio-air-quality
AWS_ACCESS_KEY_ID=<aws_access_key_for_s3>
AWS_SECRET_ACCESS_KEY=<aws_access_secret_for_s3>
```

The `docker-compose.yml` file facilitates local development by reading your `.env` file and passing environment variables to the container. `DATA_PROCESS_PERIOD_MINUTES=0` ensures script is run once and exit (no need for periodical execution on localhost - if you want to test it change value to non zero float, e.g. 0.1 = execute every 6s). `docker-compose.yml` also maps project sources to a volume, so container does not need to be rebuilt when changing the application logic - files inside `./src`

To start the project run
```
docker-compose up
```

To build the container and run tests:
```
docker build -t demo .
docker run --rm demo pytest -v
```

If you for some reason don't want to use Docker locally, you can set up a virtual environment and install necessary dependencies:

```
python3 -m venv venv
pip install -r requirements.txt
```

## Deployment ##

We need two things

- S3 Bucket to store data as per task requirement
- EC2 instance with Elastic IP to host our application and poll data regularly (Elastic IP is needed for external access - deployments)

This is partially automated using CloudFormation and Ansible. Target S3 bucket with web hosting needs to be set up manually (this was not included in CloudFormation template due to time constraints).

*Note: Our application needs AWS key to programatically manipulate S3 content - I recommend creating separate IAM role in your AWS console for this and don't use your root AWS credentials.*

### Create S3 Bucket with web hosting enabled ###

1. Go to S3 in your favorite location - [S3 Buckets](https://s3.console.aws.amazon.com/s3/home?region=eu-central-1#)
2. Create bucket with default setting and your chosen name (we use `golemio-air-quality`).
3. Unblock public access under `Permissions` tab and setup bucket policy to allow all:
   ```
   {
       "Version": "2012-10-17",
       "Statement": [
          {
               "Sid": "PublicReadGetObject",
               "Effect": "Allow",
               "Principal": "*",
               "Action": "s3:GetObject",
               "Resource": "arn:aws:s3:::golemio-air-quality/*"
           }
       ]
   }
   ```
4. Under `Properties > Static website hosting` enable hosting and name index document `index.html` (you may want to get a copy of the target URL here)

### Provision EC2 stack ###

Prerequisite is to have SSH key for your AWS EC2.

1. Go to AWS CloudFormation in your favorite region - [Create Stack](https://eu-central-1.console.aws.amazon.com/cloudformation/home?region=eu-central-1#/stacks/create/template)
2. Use `provision_ec2_in_vpc.cf.yml` as a stack template.
3. Specify stack details
   - name
   - instance type
   - ssh key
4. After stack is created, you will get public IP in the output tab - you will need this for Ansible's `inventory` file.

CloudFormation Designer representation of the provisioning script.

![](doc/cf.png)

VPC network and EC2 itself has some basic firewall rules set up:

- INBOUND allow 22, 80; deny all other; ephemeral ports allowed
- OUTBOUND allow 22, 80, 443; deny all other; ephemeral ports allowed



### CI/CD ###

Code is hosted on Bitbucket and has CI/CD pipeline configured - see content of `./bitbucket-pipelines.yml`.

- Any updates to pull requests will trigger build and test pipeline.
- Any update to master (i.e. merging pull request) will trigger deployment to EC2 instance.

In Bitbucket I've generated deployment SSH key and have configured repository wide secret variable INVENTORY containing base64 encoded content of Ansible's inventory used in the deployment to production.

This is how pipelines look in action:

https://bitbucket.org/vasekch/interview-demo/addon/pipelines/home

![](doc/pipelines.png)

We also mark one pipeline as Deployment, so we can examine this in separate view in Bitbucket:

https://bitbucket.org/vasekch/interview-demo/addon/pipelines/deployments

![](doc/deployment.png)


#### Ansible ####

*Note: These Ansible playbooks are designed for Amazon Linux (1). They may have issues runing on other distributions.*

This can be run from any environment (local or build) given all ENV vars are provided and `inventory` file is at disposal.

All commands are to be executed from within `./ansible/` directory

Copy `inventory.tpl` to `inventory` and include your own values

- IP address of EC2 server (from provisioning)
- path to EC2 SSH access private key (ssh with key enabled)
- bitbucket pipeline SSH deployment public key (to put to `~/.ssh/authorized_key` on the server to allow Bitbucket pipeline in)
- all vars from `.env` used for local development


Execute playbook for provisioning, this will ensure all is installed and needs to be run just once.

```
ansible-playbook -i inventory provision.yml
```

To update the application code run update, this it to be run every time you need to deploy new version.

```
ansible-playbook -i inventory update.yml
```

See individual steps in `./ansible/provision.yml` and `./ansible/update.yml` for more details on steps taken.

### Go to production ###

Here comes some (not all) ideas to consider before going to production - supporting the application in longterm.

Security:

- SSL for S3 webhosting: CloudFront and Amazon Certificate Manager to host content under HTTPS (create CNAME to S3 bucket in our DNS)
- Review network security for EC2 - allowed IPs, allowed ports, IPv6, assign FQDN (prerequisite for HTTPS), configure DNS, PTR; SSL (let's encrypt) and possible load ballancing / failover / HA - depends on non-functional requirements

Infrastructure:

- Use Amazon Linux 2 ami for provisioning, current Amazon Linux (1) is outdated and don't have Python3 available in yum
- consider container orchestration Rancher, Kubernetes and/or server less setup using docker as a service - GKE, ECS (lower cost of ownership)
- use S3 in multiple regions for global UX consistence (content delivery network)

Application - DevOps practices:

- Introduce more debugging features (Mock S3) and deployment environments (Dev, Test, Staging) for better quality assurance
- replace AWS CloudFormation with Hashicorp's Terraform to be cloud agnostic
- Refactor Ansible inventory to follow [good practice](https://docs.ansible.com/ansible/2.3/intro_inventory.html#splitting-out-host-and-group-specific-data)- use Docker image Registry (DockerHub, Harbor), do not build on target server
- Use monitoring for server status page
- Scan Docker images for vulnerabilities - static code analysis (SonarCube)
- Enforce code linting in the CI/CD

## Disclaimer ##

This project is part of an interview taken place in May 2020 in Prague.

Code is released under MIT license.